variable "my_tag" {
    description = "Name to tag all elements with"
    default = "shannon"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.5.0/25"
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "eu-west-2"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.5.0/27"
}

variable "public_subnet_cidr_2" {
    description = "CIDR for the Public Subnet 2"
    default = "10.0.5.96/27"
}