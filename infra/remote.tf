provider "aws" {
    shared_credentials_files = ["/Users/Shannon/Downloads/credentials"]
    profile = "Academy"
    region = "eu-west-2"
}

terraform {
    backend "s3" {
        encrypt = true
        bucket = "shannon-s3-bucket-terraform"
        key    = "infra.tfstate-academy"
        region = "eu-west-2"
        shared_credentials_file = "/Users/Shannon/Downloads/credentials"
        profile = "Academy"
    }
}
output "vpc_id" {
  value = aws_vpc.my-vpc.id
}

output "public_subnet_id" {
  value = aws_subnet.public-subnet.id
}

output "public_subnet_id_2" {
  value = aws_subnet.public-subnet-2.id
}
