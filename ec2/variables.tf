variable "ubuntu_ami" {
	description = "ubuntu ami in London or my Packer AMI"
	default = "ami-0f9124f7452cdb2a6"
}

variable "my_tag" {
		description = "Name to tag all elements with"
		default = "shannon"
}

variable "ec2_instance_type" {
	description = "Instance type of my server"
	default = "t2.micro"
}

variable "my_cidr" {
	description = "My IP address"
}

variable "aws_key_name" {
	description = "AWS Keyname for SSH"
	default = "shannon-tf-london-2"
}