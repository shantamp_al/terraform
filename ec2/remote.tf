provider "aws" {
    shared_credentials_files = ["/Users/Shannon/Downloads/credentials"]
    profile = "Academy"
    region = "eu-west-2"
}

data "terraform_remote_state" "my-infrastructure"{
    backend = "s3"
    config = {
        bucket = "shannon-s3-bucket-terraform"
        key    = "infra.tfstate-academy"
        region = "eu-west-2"
        shared_credentials_file = "/Users/Shannon/Downloads/credentials"
        profile = "Academy"
    }
}